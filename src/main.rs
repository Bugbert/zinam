use std::fs;
use std::path::PathBuf;
use serde_json::{Value, json};
use clap::Parser;
use anyhow::{anyhow, Result};

use freedesktop_desktop_entry::{default_paths, DesktopEntry, Iter};

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Narrow to names that contain a pattern
    #[arg(short, long, value_name = "PATTERN")]
    pattern: Option<String>,
    /// Set a minimum number of entries
    #[arg(short, long, value_name = "VALUE")]
    min: Option<usize>
}

fn main() {
    let args = Args::parse();

    let pattern = &match args.pattern {
        Some(pattern) => pattern,
        None => String::new(),
    }[..];
    let min = match args.min {
        Some(min) => min,
        None => 0,
    };

    let mut items: Vec<Value> = Vec::new();
    
    // Looks at all the .destop files and if our get_item function sucseeds,
    // push it to the items vector
    for path in Iter::new(default_paths()) {
        if let Ok(item) = get_item(&path, pattern) {
            items.push(item);
        }
    }

    // Ensure the minium entry requirement is met by pushing blank items
    // until there is enough
    while items.len() < min {
        items.push(json!({
            "name": "",
            "exec": ""
        }))
    }
    println!("{}", json!(items).to_string());
}

// Returns a json value for the desktop entry or an error
fn get_item(path: &PathBuf, pattern: &str) -> Result<Value> {
    let bytes = fs::read_to_string(&path)?;
    let entry = DesktopEntry::decode(path, &bytes)?;

    match parse_entry(&entry, pattern) {
        Some(item) => Ok(item),
        None => Err(anyhow!("Entry did not meet minimum requirements"))
    }
}

// Turns the DesktopEntry into a json value
fn parse_entry(entry: &DesktopEntry, pattern: &str) -> Option<Value> {
    let name = &entry.name(None)?;
    let exec = &entry.exec()?;

    // Checks if it matches the pattern, (case insensitive). If not, return None
    if !name.to_lowercase().contains(&pattern.to_lowercase()[..]) {
        return None;
    }

    // Turns the name and exec into a json value
    Some(json!({
        "name": name,
        "exec": exec
    }))
}